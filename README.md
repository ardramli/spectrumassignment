# iOS Coding Exercise
---

## Overview
A club contains companies and each company has members attached to it.
Create an application that will display the list of companies and list of members in the club.

## Goals
1. Fetch a list of [company and members](https://next.json-generator.com/api/json/get/Vk-LhK44U) (each member belongs to a company).
2. Items must contain: 
   • Company: name, logo, website, company description
   • Member: name, age, phone, email, age
3. Each list should appear in a separate view.

### Members List:
1. List must support the ability to sort by age / name.
2. Search by member name.
3. Mark a member as favorite.

### Company List:
1. List must support the ability to sort by name.
2. Search by company name.
3. Mark a company as favorite.
4. Follow a company.
