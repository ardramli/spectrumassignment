//
//  CompanyTableViewController.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 12/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit
import RealmSwift

class CompanyTableViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var companies: [CompanyModel] = [] {
        didSet {
            companies = companies.sorted(by: { $0.name.lowercased() < $1.name.lowercased() })
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    var currentCompanies = [CompanyModel]()
    var currentRLMCompanies = [RLMCompany]()
    var rlmCompanies = [RLMCompany]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefault()
        getDataFromRealm()
        
        if rlmCompanies.isEmpty {
            getCompanies()
        }
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    func setupDefault() {
        searchBar.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    func getCompanies() {
        let companyAPI = CompanyAPI()
        companyAPI.getCompanies { [weak self] result in
            switch result {
            case .success(let club):
                self?.companies = club.map({ CompanyModel.initWithCompany(company: $0) })
                if let companies = self?.companies {
                    self?.currentCompanies = companies
                    for company in companies {
                        self?.saveToRealm(company: company)
                    }
                    self?.getDataFromRealm()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc
    func hideKeyboard() {
        searchBarCancelButtonClicked(searchBar)
    }
    
    func saveToRealm(company: CompanyModel) {
        if rlmCompanies.isEmpty {
            let realmUtil = RealmUtil()
            if let realmCompany = RLMCompany(companyModel: company) {
                realmUtil.addRealmObject(realmObject: realmCompany)
            }
        }
    }
    
    func getDataFromRealm() {
        DispatchQueue.main.async {
            let realmUtil = RealmUtil()
            self.rlmCompanies = realmUtil.getRealmObjects(realmObjectType: RLMCompany.self).toArray()
            self.currentRLMCompanies = self.rlmCompanies
            self.tableView.reloadData()
        }
    }
    
    @objc
    func favSwitchToggle(_ sender: UISwitch) {
        let realmUtil = RealmUtil()
        let company = self.currentRLMCompanies[sender.tag]
        do {
            try realmUtil.realm.write {
                company.isFavorite = sender.isOn
            }
        } catch {
            print("error when writing to realmCompany")
        }
        realmUtil.updateRealmObject(realmObject: company)
    }
    
    @objc
    func followSwitchToggle(_ sender: UISwitch) {
        let realmUtil = RealmUtil()
        let company = self.currentRLMCompanies[sender.tag]
        do {
            try realmUtil.realm.write {
                company.isFollowed = sender.isOn
            }
        } catch {
            print("error when writing to realmCompany")
        }
        realmUtil.updateRealmObject(realmObject: company)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentRLMCompanies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as? CompanyCell else {
            return UITableViewCell()
        }
        let company = self.currentRLMCompanies[indexPath.row]
        cell.nameLabel.text = company.name
        cell.descriptionLabel.text = company.about
        cell.websiteLabel.text = company.website
        cell.logoImageView.setImageWithURLString(company.logo, shouldFadeIn: true)
        cell.selectionStyle = .none
        
        cell.followSwitch.isOn = company.isFollowed
        cell.followSwitch.tag = indexPath.row
        cell.followSwitch.addTarget(self, action: #selector(followSwitchToggle(_:)), for: .valueChanged)
        
        cell.favSwitch.isOn = company.isFavorite
        cell.favSwitch.tag = indexPath.row
        cell.favSwitch.addTarget(self, action: #selector(favSwitchToggle(_:)), for: .valueChanged)
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCompany = currentRLMCompanies[indexPath.row]
        if let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MembersViewController") as? MembersViewController {
            vc.members = selectedCompany.members.toArray()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CompanyTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentRLMCompanies = rlmCompanies
            tableView.reloadData()
            return
        }
        currentRLMCompanies = rlmCompanies.filter { company -> Bool in
            guard let text = searchBar.text else { return false }
            return company.name.lowercased().contains(text.lowercased())
        }
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        currentRLMCompanies = rlmCompanies
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        tableView.reloadData()
    }
}

