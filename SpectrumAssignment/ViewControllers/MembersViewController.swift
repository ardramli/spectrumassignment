//
//  MembersViewController.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit

class MembersViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var members = [RLMMember]()
    var currentMembers = [RLMMember]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
    }
    
    func setupDefaults() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        searchBar.delegate = self
        currentMembers = members.sorted(by: { $0.age < $1.age })
        self.title = "Members"
        searchBar.selectedScopeButtonIndex = 0
    }
    
    @objc
    func favSwitchToggle(_ sender: UISwitch) {
        let realmUtil = RealmUtil()
        let member = self.currentMembers[sender.tag]
        do {
            try realmUtil.realm.write {
                member.isFavorite = sender.isOn
            }
        } catch {
            print("error when writing to realmMember")
        }
        realmUtil.updateRealmObject(realmObject: member)
    }
}

//MARK: - table view cell data source and delegate
extension MembersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell", for: indexPath) as? MemberCell else {
            return UITableViewCell()
        }
        let member = self.currentMembers[indexPath.row]
        cell.nameLabel.text = member.name
        cell.ageLabel.text = member.age
        cell.phoneLabel.text = member.phone
        cell.emailLabel.text = member.email
        cell.selectionStyle = .none
        cell.favSwitch.isOn = member.isFavorite
        cell.favSwitch.tag = indexPath.row
        cell.favSwitch.addTarget(self, action: #selector(favSwitchToggle), for: .valueChanged)
        return cell
    }
}

//MARK: - search bar delegate
extension MembersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentMembers = members
            searchBar.selectedScopeButtonIndex = 0
            tableView.reloadData()
            return
        }
        currentMembers = members.filter { member -> Bool in
            guard let text = searchBar.text else { return false }
            let fullName = member.name
            return fullName.lowercased().contains(text.lowercased())
        }
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        switch selectedScope {
        case 0: //sort by name
            currentMembers = members.sorted(by: { $0.age < $1.age })
            tableView.reloadData()
        case 1: //sort by age
            currentMembers = members.sorted(by: { $0.name < $1.name })
            tableView.reloadData()
        default:
            break
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        currentMembers = members
        searchBar.selectedScopeButtonIndex = 0
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        tableView.reloadData()
    }
}
