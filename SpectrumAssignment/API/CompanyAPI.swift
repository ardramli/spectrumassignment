//
//  ClubAPI.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 12/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import Foundation

enum ClubError: Error {
    case noDataAvailable
    case cannotProcessData
}

struct CompanyAPI {
    let resourceURL: URL
    
    init() {
        let urlString = "https://next.json-generator.com/api/json/get/Vk-LhK44U"
        guard let url = URL(string: urlString) else { fatalError() }
        resourceURL = url
    }
    
    func getCompanies(completion: @escaping(Result<Companies, ClubError>) -> Void) {
        let task = URLSession.shared.clubTask(with: resourceURL) { clubData, _, _ in
            if let club = clubData {
                completion(.success(club))
            } else {
                completion(.failure(.noDataAvailable))
            }
        }
        task.resume()
    }
}

// MARK: - Helper functions for creating decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

    func clubTask(with url: URL, completionHandler: @escaping (Companies?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
