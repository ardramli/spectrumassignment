//
//  RealmUtil.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import Foundation
import RealmSwift

protocol DetachableObject: AnyObject {
    func detached() -> Self
    func detachedRecursively() -> Self
}

public struct RealmUtil {

    var realm: Realm = try! Realm()
    
    // MARK: GET Realm Objects
    public func getRealmObjects<T: Object> (realmObjectType: T.Type, predicate: String? = nil, sortProperty: String? = nil, isAscending: Bool = true) -> Results<T> {
        var results: Results<T>
        
        if let pred = predicate {
            results = realm.objects(realmObjectType).filter(pred)
        } else {
            results = realm.objects(realmObjectType)
        }
        
        if let property = sortProperty {
            results = results.sorted(byKeyPath: property, ascending: isAscending)
        }
        return results
    }
    
    // MARK: CREATE / UPDATE Realm Object
    public func addRealmObject<T: Object>(realmObject: T) {
        do {
            try realm.write {
                realm.add(realmObject)
            }
        } catch {
            print("Unexpected error: \(error.localizedDescription)")
        }
    }

    public func updateRealmObject<T: Object>(realmObject: T) {
        do {
            try realm.write {
                realm.add(realmObject)
            }
        } catch {
            print("Unexpected error: \(error.localizedDescription)")
        }
    }
}


// MARK: List extension
extension List: DetachableObject {
    func detachedRecursively() -> List<Element> {
        return detached(recursive: true)
    }

    func detached() -> List<Element> {
        return detached(recursive: false)
    }

    func detached(recursive: Bool) -> List<Element> {
        let result = List<Element>()

        forEach {
            if let detachable = $0 as? DetachableObject, recursive {
                if let detached = detachable.detachedRecursively() as? Element {
                    result.append(detached)
                } else {
                    assert(false, "Invalid detached")
                }
            } else {
                result.append($0) //Primtives are pass by value; don't need to recreate
            }
        }

        return result
    }

    func toArray() -> [Element] {
        return Array(detached())
    }
}

// MARK: Results extension
extension Results {
    func toArray() -> [Element] {
        let result = List<Element>()

        forEach {
            result.append($0)
        }

        return Array(result.detached())
    }
}
