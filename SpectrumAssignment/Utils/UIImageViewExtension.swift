//
//  UIImageViewExtension.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func setImageWithURLString(_ string: String?, placeholderImage: UIImage? = nil, shouldFadeIn fade: Bool, withDuration duration: TimeInterval = 0.5, completionBlock: (() -> Void)? = nil) {

        if let string = string {
            if let url: URL = URL(string: string) {
                let cacheKey = SDWebImageManager.shared.cacheKey(for: url)
                SDImageCache.shared.containsImage(forKey: cacheKey, cacheType: .memory) { (type: SDImageCacheType) in
                    if type == .none {
                        self.alpha = 0
                    } else {
                        self.alpha = 1
                    }
                }

                sd_setImage(with: url, placeholderImage: placeholderImage, options: SDWebImageOptions.retryFailed, completed: { (_: UIImage?, _: Error?, type: SDImageCacheType, _: URL?) -> Void in
                    completionBlock?()
                    if type == .none {
                        UIView.animate(withDuration: duration, animations: {
                            self.alpha = 1
                        })
                    } else {
                        self.alpha = 1
                    }
                })

                return
            }
        }
        if fade {
            UIView.animate(withDuration: duration, animations: { self.alpha = 1 })
        }
    }
}
