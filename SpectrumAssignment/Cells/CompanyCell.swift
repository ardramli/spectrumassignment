//
//  CompanyCell.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var followSwitch: UISwitch!
    @IBOutlet weak var favSwitch: UISwitch!
}
