//
//  MemberCell.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit

class MemberCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var favSwitch: UISwitch!
}
