//
//  APIResponse.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 12/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import Foundation

// MARK: - Company
struct Company: Codable {
    let id, company, website: String
    let logo: String
    let about: String
    let members: [Member]

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case company, website, logo, about, members
    }
}

struct CompanyModel {
    var id: String
    var name: String
    var logo: String
    var website: String
    var about: String
    var members: [MemberModel]
    var isFollowed: Bool
    var isFavourite: Bool
    
    static func initWithCompany(company: Company) -> CompanyModel {
        let membersModelArray = company.members.map({ MemberModel.initWithMember(member: $0 )})
        let companyModel = CompanyModel(id: company.id,
                                        name: company.company,
                                        logo: company.logo,
                                        website: company.website,
                                        about: company.about,
                                        members: membersModelArray,
                                        isFollowed: false,
                                        isFavourite: false)
        return companyModel
    }
}

// MARK: - Member
struct Member: Codable {
    let id: String
    let age: Int
    let name: Name
    let email, phone: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case age, name, email, phone
    }
}

struct MemberModel {
    var id: String
    var age: String
    var name: String
    var email: String
    var phone: String
    var isFavourite: Bool
    
    static func initWithMember(member: Member) -> MemberModel {
        let fullName = member.name.first + " " + member.name.last
        let memberModel = MemberModel(id: member.id,
                                      age: "\(member.age)",
                                      name: fullName,
                                      email: member.email,
                                      phone: member.phone,
                                      isFavourite: false)
        return memberModel
    }
}

// MARK: - Name
struct Name: Codable {
    let first, last: String
}

typealias Companies = [Company]
