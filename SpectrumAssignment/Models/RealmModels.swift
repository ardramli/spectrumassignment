//
//  RealmModels.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 13/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import Foundation
import RealmSwift

class RLMCompany: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var website: String = ""
    @objc dynamic var about: String = ""
    @objc dynamic var logo: String = ""
    @objc dynamic var isFollowed: Bool = false
    @objc dynamic var isFavorite: Bool = false
    var members: List<RLMMember> = List<RLMMember>()
    
    convenience init?(companyModel: CompanyModel) {
        self.init()
        about = companyModel.about
        name = companyModel.name
        website = companyModel.website
        logo = companyModel.logo
        isFollowed = companyModel.isFollowed
        isFavorite = companyModel.isFavourite

        for member in companyModel.members {
            if let rlmMember = RLMMember(memberModel: member) {
                members.append(rlmMember)
            }
        }
    }
}

class RLMMember: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var age: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var isFavorite: Bool = false
    
    convenience init?(memberModel: MemberModel) {
        self.init()
        name = memberModel.name
        age = memberModel.age
        email = memberModel.email
        phone = memberModel.phone
        isFavorite = memberModel.isFavourite
    }
}
