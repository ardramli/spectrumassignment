//
//  AppDelegate.swift
//  SpectrumAssignment
//
//  Created by Adli Ramli on 12/06/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupRealm()
        return true
    }
    
    // MARK: Realm Setup
    func setupRealm() {

        var currentSchemaVersion: UInt64 = 0

        do {
            if let realmFileURL = RLMRealmConfiguration.default().fileURL {
                currentSchemaVersion = try schemaVersionAtURL(realmFileURL)
            }

            let currentConfig = Realm.Configuration(
                schemaVersion: currentSchemaVersion,
                migrationBlock: nil
            )

            Realm.Configuration.defaultConfiguration = currentConfig

            let realm = try Realm()
            
        } catch let realmError as RLMError {
            if realmError.code == RLMError.schemaMismatch {
                print("schemaMismatch thrown on Realm:\n \(realmError.localizedDescription)")
                performRealmMigration(currentSchemaVersion: currentSchemaVersion)
            } else {
                print("unhandled Realm Error caught:\n \(realmError.localizedDescription)")
            }
        } catch {
            print("error initializing Realm:\n \(error.localizedDescription)")
        }

        print("Realm: \(RLMRealmConfiguration.default().fileURL?.absoluteString ?? "")")
    }
    
    func performRealmMigration(currentSchemaVersion: UInt64) {
        let migrationBlock: MigrationBlock = { migration, oldSchemaVersion in
            if oldSchemaVersion < 1 {
                // do any form of data massaging here, such as renaming column names etc etc
            }
        }

        var newConfig = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: currentSchemaVersion + 1,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: migrationBlock
        )

        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = newConfig

        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        do {
            let realm = try Realm()
            print("Realm New Schema Version: \(realm.configuration.schemaVersion)")
        } catch {
            print("error migrating Realm:\n \(error.localizedDescription)")
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

